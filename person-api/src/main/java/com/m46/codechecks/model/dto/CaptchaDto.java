package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class CaptchaDto {
    private Long questionId;
    private String answer;
    private Long personId;
}
