package com.m46.codechecks.repository;

import com.m46.codechecks.model.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaptchaRepository extends JpaRepository<QuestionEntity,Long> {

}
