package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.AnswerDto;
import com.m46.codechecks.model.dto.QuestionDto;
import com.m46.codechecks.service.CaptchaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/captcha")
public class CaptchaController {

    private final CaptchaService captchaService;

    @GetMapping(path = "/get")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<QuestionDto> get() {
        return ResponseEntity.ok(captchaService.getQuestion());
    }

    @PostMapping(path = "/post")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AnswerDto> check(@RequestBody AnswerDto answer) throws RuntimeException {
        return ResponseEntity.ok(captchaService.postAnswer(answer));
    }

}
