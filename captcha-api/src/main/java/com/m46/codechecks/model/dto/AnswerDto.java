package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class AnswerDto {
    private Long questionId;
    private String answer;
}
